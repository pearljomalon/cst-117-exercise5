namespace Exercise5
{
    partial class ApproxPiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.enterLabel = new System.Windows.Forms.Label();
            this.termsTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.termLabel = new System.Windows.Forms.Label();
            this.approxLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // enterLabel
            // 
            this.enterLabel.AutoSize = true;
            this.enterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.enterLabel.Location = new System.Drawing.Point(28, 38);
            this.enterLabel.Name = "enterLabel";
            this.enterLabel.Size = new System.Drawing.Size(119, 18);
            this.enterLabel.TabIndex = 0;
            this.enterLabel.Text = "Enter #of Terms:";
            // 
            // termsTextBox
            // 
            this.termsTextBox.Location = new System.Drawing.Point(196, 35);
            this.termsTextBox.Name = "termsTextBox";
            this.termsTextBox.Size = new System.Drawing.Size(100, 20);
            this.termsTextBox.TabIndex = 1;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(33, 101);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 2;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // termLabel
            // 
            this.termLabel.AutoSize = true;
            this.termLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.termLabel.Location = new System.Drawing.Point(30, 157);
            this.termLabel.Name = "termLabel";
            this.termLabel.Size = new System.Drawing.Size(0, 18);
            this.termLabel.TabIndex = 3;
            // 
            // approxLabel
            // 
            this.approxLabel.AutoSize = true;
            this.approxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.approxLabel.Location = new System.Drawing.Point(33, 192);
            this.approxLabel.Name = "approxLabel";
            this.approxLabel.Size = new System.Drawing.Size(0, 18);
            this.approxLabel.TabIndex = 4;
            // 
            // ApproxPiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 232);
            this.Controls.Add(this.approxLabel);
            this.Controls.Add(this.termLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.termsTextBox);
            this.Controls.Add(this.enterLabel);
            this.Name = "ApproxPiForm";
            this.Text = "Approximate PI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label enterLabel;
        private System.Windows.Forms.TextBox termsTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label termLabel;
        private System.Windows.Forms.Label approxLabel;
    }
}

