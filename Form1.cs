using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise5
{
    public partial class ApproxPiForm : Form
    {
        public ApproxPiForm()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            int numberTerms;

            if (int.TryParse(termsTextBox.Text, out numberTerms))
            {
                double pi = 4;

                if (numberTerms == 1)
                {
                    termLabel.Text = "Approximate value of Pi after 1 term: ";
                }

                else if (numberTerms > 1 && numberTerms <= int.MaxValue)
                {
                    long denominator = 3;
                    bool addSubtract = false;

                    termLabel.Text = "Approximate value of Pi after " + numberTerms.ToString() + " terms:";

                    for (int i = 2; i <= numberTerms; i++)
                    {
                        switch (addSubtract)
                        {
                            case false:
                                pi = pi - ((double)4 / denominator);
                                denominator += 2;
                                addSubtract = !addSubtract;
                                break;

                            case true:
                                pi = pi + ((double)4 / denominator);
                                denominator += 2;
                                addSubtract = !addSubtract;
                                break;
                        }
                    }
                }

                approxLabel.Text = pi.ToString();
            }

            else
            {
                MessageBox.Show("Invalid input for the number of terms. Please enter a whole number between 1 and " + int.MaxValue.ToString());
            }
        }
    }
}
